import React from 'react';
import { Image } from 'semantic-ui-react';
import ComingSoonImg from '../img/coming_soon.png';


const ComingSoon = ({ message }) => {
    return (
        <div style={{ 'marginTop': '3.5em' }}>
            <h1>{message}</h1>
            <Image src={ComingSoonImg}/>
        </div>
    );
};


export default ComingSoon;
