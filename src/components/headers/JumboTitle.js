import React from 'react';


const JumboTitle = ({ text }) => {

    const PageTitleStyle = {
        'fontSize': '3.5em'
    };

    return (
        <h1 style={PageTitleStyle}>{text}</h1>
    );
};


export default JumboTitle;
