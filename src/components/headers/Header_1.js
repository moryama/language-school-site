import React from 'react';
import { Header } from 'semantic-ui-react';


const Header_1 = ({ text }) => {
    return (
        <Header size='large'>{text}</Header>
    );
};


export default Header_1;
