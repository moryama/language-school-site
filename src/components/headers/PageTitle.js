import React from 'react';
import { capitalize } from '../../utils';


const PageTitle = ({ text }) => {

    const PageTitleStyle = {
        'fontSize': '3em',
        'textAlign': 'center',
        'marginBottom': '1.3em'
    };

    return (
        <h1 style={PageTitleStyle}>{capitalize(text)}</h1>
    );
};


export default PageTitle;
