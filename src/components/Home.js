import React from 'react';
import { Image, Message } from 'semantic-ui-react';
import HomeImg from '../img/home.jpg';


const Home = () => {

    const JumbotronStyle = {
        'margin': '0',
        'padding': '0'
    };

    return (
        <>
            <Message
                style={JumbotronStyle}
                size='massive'
            >
                <Image src={HomeImg}/>
            </Message>
        </>
    );
};


export default Home;