export const text = {
    'motto': 'Vivi appieno l\'italiano',
    'plida': [
        {
            'id': 1,
            'label': 'Cos\'è il Plida'
        },
        {
            'id': 2,
            'label': 'Prezzi e calendario'
        },
        {
            'id': 3,
            'label': 'Iscrizione all\'esame'
        },
        {
            'id': 4,
            'label': 'Corsi di preparazione'
        }
    ],
    'contact':
    {
        'loc': {
            'address': 'Via Estate 34, 3 piano, con ascensore',
            'tram': 'tram: 1, 8, 26',
            'metro': 'metro: Linea B, Hradcanska',
            'parking': 'parcheggio: libero nel piazzale davanti al parco'
        },
        'time': {
            'title': 'Segreteria aperta:',
            'am': 'Mart-Merc-Giov 8:30-9:00',
            'pm': '17:30-19:00'
        },
        'email': {
            'info': 'info@scuola.cz',
            'ita': 'corsi@scuola.cz',
            'cz': 'corsidiceco.scuola.cz'
        },
        'phone': {
            'info': '456 123 789',
            'ita': '123 123 789',
            'cz': '456 123 123',
        },
        'legalLoc': {
            'address': 'Nevsky prospekt 12',
            'ICO': 'ICO 324798173592'
        }
    }
};

export const colors = {
    'danteRed': '#bf2e1a',
    'lightBlue': '#E4EFF9'
};
