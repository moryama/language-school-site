import React from 'react';
import { useSelector } from 'react-redux';
import {
    Switch,
    Route,
    useRouteMatch
} from 'react-router-dom';
import { toSnake } from '../utils';
import ContactView from './contact/ContactView';
import Course from './courses/Course';
import CoursesView from './courses/CoursesView';
import PlidaView from './plida/PlidaView';
import { Container } from 'semantic-ui-react';
import Breadcrumb from './nav/Breadcrumb';
import ComingSoon from './ComingSoon';


const Content = () => {
    const courses = useSelector(({ courses }) => {return courses; });

    const match = useRouteMatch('/corsi/:language/:name');
    const course = match
        ? courses.find(course => toSnake(course.labelPl) === match.params.name)
        : null;

    const ContentStyle = {
        'margin': '1.2em, 0, 1.2em, 0'
    };

    return (
        <div style={ContentStyle}>
            <Breadcrumb />
            <Switch>
                <Route path='/corsi' exact>
                    <Container>
                        <CoursesView />
                    </Container>
                </Route>
                <Route path='/corsi/:language/:name'>
                    <Container>
                        <Course course={course} />
                    </Container>
                </Route>
                <Route path='/plida'>
                    <Container>
                        <PlidaView />
                    </Container>
                </Route>
                <Route path='/contatti'>
                    <Container>
                        <ContactView />
                    </Container>
                </Route>
                <Route path='/chi_siamo'>
                    <Container>
                        <ComingSoon message='Pagina in costruzione'/>
                    </Container>
                </Route>
                <Route path='/novità'>
                    <Container>
                        <ComingSoon message='Pagina in costruzione'/>
                    </Container>
                </Route>
            </Switch>
            <Breadcrumb />
        </div>
    );
};


export default Content;