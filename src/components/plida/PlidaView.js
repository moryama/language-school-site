import React from 'react';
import PageTitle from '../headers/PageTitle';
import { Grid, Image } from 'semantic-ui-react';
import plidaImg from '../../img/exam.jpg';
import { text } from '../constants';


const PlidaView = () => {
    const plidaLinks = text.plida;

    if (!plidaLinks) {
        return (
            <div>Pagina in costruzione...</div>
        );
    }

    return (
        <div>
            <PageTitle text='Esame Plida'/>
            <Grid columns={3}>
                <Grid.Column>
                    {
                        plidaLinks.map(link =>
                            <p key={link.id} style={{ 'fontSize': '1.3em' }}>
                                <a href={`#${link.id}`}>{link.label}</a>
                            </p>)
                    }
                </Grid.Column>
                <Grid.Column></Grid.Column>
                <Grid.Column>
                    <Image src={plidaImg}/>
                </Grid.Column>
            </Grid>

        </div>
    );
};


export default PlidaView;