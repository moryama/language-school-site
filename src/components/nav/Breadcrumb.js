import React from 'react';
import { Breadcrumb as BrCr } from 'semantic-ui-react';
import { useLocation } from 'react-router-dom';
import { splitPath } from '../../utils';
import { nanoid } from 'nanoid';


const Breadcrumb = () => {

    const BreadcrumbStyle = {
        'margin': '2em'
    };

    const currLocation = useLocation();
    const pathItems = splitPath(currLocation.pathname);

    return (
        <BrCr size='big' style={BreadcrumbStyle}>
            <BrCr.Section>home</BrCr.Section>
            <BrCr.Divider icon='right chevron' />
            {
                pathItems.map(item =>
                    <React.Fragment key={nanoid()}>
                        <BrCr.Section>{item}</BrCr.Section>
                        <BrCr.Divider icon='right chevron' />
                    </React.Fragment>
                )
            }
        </BrCr>
    );
};


export default Breadcrumb;