import React from 'react';
import { Dropdown } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { toSnake } from '../../utils';


const CoursesMenu = ({ courses }) => {

    return (
        <>
            <Dropdown.Header
                key={courses.language}>
                {courses.language}
            </Dropdown.Header>
            {
                courses.courses.map(course =>
                    <Dropdown.Item
                        key={course.id}
                        as={Link}
                        to={`/corsi/${course.language}/${toSnake(course.labelPl)}`}
                    >
                        {course.labelPl}
                    </Dropdown.Item>
                )
            }
            <Dropdown.Divider />
        </>
    );
};


export default CoursesMenu;