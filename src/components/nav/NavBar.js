import React from 'react';
import { useSelector } from 'react-redux';
import { Dropdown, Menu } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { colors } from '../constants';
import CoursesMenu from './CoursesMenu';


const NavBar = () => {
    const courses = useSelector(({ courses }) => {return courses; });

    const ItalianCourses = courses.filter(course => course.language === 'italiano');
    const CzechCourses = courses.filter(course => course.language === 'ceco');

    const coursesByLang = [
        {
            language: 'italiano',
            courses: ItalianCourses
        },
        {
            language: 'ceco',
            courses: CzechCourses
        }
    ];

    return (
        <Menu
            size='massive'
            inverted
            style={{ 'backgroundColor': colors.danteRed, 'margin': '0' }}
        >
            <Menu.Menu position='right'>
                <Menu.Item name='chi siamo' as={Link} to='/chi_siamo'></Menu.Item>
                <Menu.Item name='novità' as={Link} to='/novità'></Menu.Item>
                <Dropdown item simple text='Corsi'>
                    <Dropdown.Menu>
                        {
                            coursesByLang.map(courseGroup =>
                                <CoursesMenu courses={courseGroup} key={courseGroup.language}/>
                            )
                        }
                        <Dropdown.Item as={Link} to='/corsi'>tutti i corsi</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                <Menu.Item name='plida' as={Link} to='/plida'></Menu.Item>
                <Menu.Item name='contatti' as={Link} to='/contatti'></Menu.Item>
            </Menu.Menu>
        </Menu>
    );
};


export default NavBar;