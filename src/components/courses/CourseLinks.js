import React from 'react';
import Header_1 from '../headers/Header_1';


const CourseLink = ({ courses, title }) => {
    return (
        <div>
            <Header_1 text={title} />
            {
                courses.map(course =>
                    <p key={course.id} style={{ 'fontSize': '1.3em' }}>
                        <a href={`#${course.id}`}>{course.labelPl}</a>
                    </p>
                )
            }
        </div>
    );
};


export default CourseLink;