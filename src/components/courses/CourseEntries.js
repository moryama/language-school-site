import React from 'react';
import { Grid } from 'semantic-ui-react';
import CourseTable from './CourseTable';
import ComingSoon from '../ComingSoon';


const CourseEntries = ({ entries }) => {

    if (!entries || entries.length < 1) {
        return <ComingSoon message={'Nuovi corsi stanno arrivando!'} />;
    }

    return (
        <>
            <Grid.Row>
                Filters panel here
            </Grid.Row>
            <Grid.Row>
                <CourseTable entries={entries} />
            </Grid.Row>
        </>
    );
};


export default CourseEntries;
