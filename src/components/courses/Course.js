import React from 'react';
import PageTitle from '../headers/PageTitle';
import { Segment } from 'semantic-ui-react';
import { Grid } from 'semantic-ui-react';
// import CourseTable from './CourseTable';
import ContactBox from '../contact/ContactBox';
import CourseEntries from './CourseEntries';


const Course = ({ course }) => {

    if (!course) {
        return (
            <div>Corsi in arrivo!</div>
        );
    }

    return (
        <>
            <PageTitle text={course.labelPl} />
            <Grid
                centered
                stackable
            >
                <Grid.Row>
                    <Grid.Column width={12}>
                        <Segment
                            size='large'
                            padded
                        >
                            {course.description}
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <ContactBox email={course.email} phone={course.phone} />
                </Grid.Row>
                <CourseEntries entries={course.entries} />
            </Grid>
        </>
    );
};


export default Course;
