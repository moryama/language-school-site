import React from 'react';
import { useSelector } from 'react-redux';
import CourseList from './CourseList';
import CourseLinks from './CourseLinks';
import { Grid, Image } from 'semantic-ui-react';
import coursesImg from '../../img/learners.jpg';
import PageTitle from '../headers/PageTitle';


const CoursesView = () => {

    const courses = useSelector(({ courses }) => {return courses; });

    if (!courses) {
        return null;
    }

    const ItalianCourses = courses.filter(course => course.language === 'italiano');
    const CzechCourses = courses.filter(course => course.language === 'ceco');

    const coursesByLang = [
        {
            language: 'italiano',
            courses: ItalianCourses
        },
        {
            language: 'ceco',
            courses: CzechCourses
        }
    ];

    return (
        <div>
            <PageTitle text='I nostri corsi'/>
            <Grid columns={3}>
                {
                    coursesByLang.map(courseGroup =>
                        <Grid.Column key={courseGroup.language}>
                            <CourseLinks
                                courses={courseGroup.courses}
                                title={`Corsi di ${courseGroup.language}`}
                            />
                        </Grid.Column>
                    )
                }
                <Grid.Column>
                    <Image src={coursesImg}/>
                </Grid.Column>

            </Grid>
            {
                coursesByLang.map(courseGroup =>
                    <CourseList
                        key={courseGroup.language}
                        courses={courseGroup.courses}
                        title={`Corsi di ${courseGroup.language}`}
                    />
                )
            }
        </div>
    );
};


export default CoursesView;
