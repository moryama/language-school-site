import React from 'react';
import { Table } from 'semantic-ui-react';
import { colors } from '../constants';


const CourseTable = ({ entries }) => {
    const HeaderStyle = {
        'background': colors.lightBlue
    };

    return (
        <Table celled>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell style={HeaderStyle}>Livello</Table.HeaderCell>
                    <Table.HeaderCell style={HeaderStyle}>Giorno</Table.HeaderCell>
                    <Table.HeaderCell style={HeaderStyle}>Orario</Table.HeaderCell>
                    <Table.HeaderCell style={HeaderStyle}>Luogo</Table.HeaderCell>
                    <Table.HeaderCell style={HeaderStyle}>Periodo</Table.HeaderCell>
                    <Table.HeaderCell style={HeaderStyle}>Prezzo</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {
                    entries.map(entry =>
                        <Table.Row key={entry.id}>
                            <Table.Cell>{entry.level}</Table.Cell>
                            <Table.Cell>{entry.day}</Table.Cell>
                            <Table.Cell>{entry.timeStart}-{entry.timeEnd}</Table.Cell>
                            <Table.Cell>{entry.place}</Table.Cell>
                            <Table.Cell>{entry.dateStart}-{entry.dateEnd}</Table.Cell>
                            <Table.Cell>{entry.price}</Table.Cell>
                        </Table.Row>
                    )
                }
            </Table.Body>
        </Table>
    );
};


export default CourseTable;
