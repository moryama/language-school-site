import React from 'react';
import { Link } from 'react-router-dom';
import { Item } from 'semantic-ui-react';
import { capitalize, toSnake } from '../../utils';
import Header_1 from '../headers/Header_1';


const CourseList = ({ courses, title }) => {
    return (
        <Item.Group>
            <Header_1 text={title} />
            {courses.map(course =>
                <Item key={course.id}>
                    <Item.Content>
                        <Item.Header id={course.id}>
                            <Link
                                to={`/corsi/${course.language}/${toSnake(course.labelPl)}`}
                                style={{ 'color': 'black' }}
                            >
                                {capitalize(course.labelPl)}
                            </Link>
                        </Item.Header>
                        <Item.Description>{course.description}</Item.Description>
                        <Item.Extra><Link to={`/corsi/${course.language}/${toSnake(course.labelPl)}`}>Scopri di più</Link></Item.Extra>
                    </Item.Content>
                </Item>
            )}
        </Item.Group>
    );
};


export default CourseList;
