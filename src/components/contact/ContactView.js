import React from 'react';
import PageTitle from '../headers/PageTitle';
import Header_1 from '../headers/Header_1';
import { Grid, Image } from 'semantic-ui-react';
import Img from '../../img/map.jpg';
import { text } from '../constants';


const ContactView = () => {
    const contact = text.contact;

    if (!contact) {
        return (
            <div>Pagina in costruzione...</div>
        );
    }

    return (
        <>
            <PageTitle text='Contatti' />
            <Grid
                stackable
                columns={3}
                divided='vertically'
                centered
            >
                <Grid.Row>
                    <Grid.Column width={4}>
                        <Header_1 text='Dove siamo' />
                    </Grid.Column>
                    <Grid.Column>
                        <p>{contact.loc.address}</p>
                        <p>{contact.loc.tram}</p>
                        <p>{contact.loc.metro}</p>
                        <p>{contact.loc.parking}</p>
                    </Grid.Column>
                    <Grid.Column>
                        <Image src={Img} />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column  width={4}>
                        <Header_1 text='Orari' />
                    </Grid.Column>
                    <Grid.Column>
                        <p>{contact.time.title}</p>
                        <p>{contact.time.am}</p>
                        <p>{contact.time.pm}</p>
                    </Grid.Column>
                    <Grid.Column>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column  width={4}>
                        <Header_1 text='Email' />
                    </Grid.Column>
                    <Grid.Column>
                        <p>{contact.email.info}</p>
                        <p>{contact.email.ita}</p>
                        <p>{contact.email.cz}</p>
                    </Grid.Column>
                    <Grid.Column>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column  width={4}>
                        <Header_1 text='Telefono' />
                    </Grid.Column>
                    <Grid.Column>
                        <p>{contact.phone.info}</p>
                        <p>{contact.phone.ita}</p>
                        <p>{contact.phone.cz}</p>
                    </Grid.Column>
                    <Grid.Column>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column  width={4}>
                        <Header_1 text='Sede legale' />
                    </Grid.Column>
                    <Grid.Column>
                        <p>{contact.legalLoc.address}</p>
                        <p>{contact.legalLoc.ICO}</p>
                    </Grid.Column>
                    <Grid.Column>
                    </Grid.Column>
                </Grid.Row>



            </Grid>



        </>




    );
};


export default ContactView;