import React from 'react';
import { Segment } from 'semantic-ui-react';


const ContactBox = ({ email, phone }) => {
    return (

        <Segment
            size='large'
            padded
            color='red'
        >
            per domande o informazioni contattare:
            <br/>
            {email}
            <br/>
            {phone}
        </Segment>


    );
};


export default ContactBox;