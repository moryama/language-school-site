/**
 * *** Note on React.StrictMode ***
 *
 * We were getting a "findDOMNode is deprecated in StrictMode" warning
 * related to the NavBar component using Semantic UI
 *
 * [check issue #2 https://gitlab.com/moryama/language-school-site/-/issues/2]
 *
 * To remove the warning, we temporarily moved <React.StrictMode>
 * to App.js to exclude the NavBar component
 *
 * We wait for updates in Semantic UI to resume strict mode here below.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import store from './redux/store';
import 'semantic-ui-css/semantic.min.css';
import { BrowserRouter } from 'react-router-dom';


ReactDOM.render(
    <Provider store={store}>
        {/* Strict mode is temporarily disabled */}
        {/* <React.StrictMode> */}
        <BrowserRouter>
            <App />
        </BrowserRouter>
        {/* </React.StrictMode> */}
    </Provider>,
    document.getElementById('root')
);
