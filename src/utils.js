/**
 * Take "a string" and return "A string"
 */
export const capitalize = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
};


/**
 * Take "a string" and return "a_string"
 */
export const toSnake = (string) => {
    return string.replaceAll(' ', '_');
};


/**
 * Take "a_string" and return "a string"
 */
export const snakeToHuman = (string) => {
    return string.replaceAll('_', ' ');
};

/**
 * Take a "url/path/like_this"
 * and return "['url', 'path', 'like this']"
 */
export const splitPath = (path) => {
    let splitItems = path.split('/');

    let splitPath = [];

    for (let i = 0; i < splitItems.length; i++) {

        if (splitItems[i] === '') {
            continue;
        } else if (splitItems[i].includes('_')) {
            splitPath.push(snakeToHuman(splitItems[i]));
        } else {
            splitPath.push(splitItems[i]);
        }
    }

    return splitPath;
};
