import React from 'react';
import './App.css';
import NavBar from './components/nav/NavBar';
import Home from './components/Home';
import { useLocation } from 'react-router-dom';
import Content from './components/Content';
import { useDispatch } from 'react-redux';
import { initializeCourses } from './redux/reducers/coursesReducer';


const App = () => {

    const currLocation = useLocation();

    const dispatch = useDispatch();
    dispatch(initializeCourses());

    return (
        <div className='site-view'>
            <NavBar />
            <React.StrictMode>
                {
                    currLocation.pathname === '/'
                        ? <Home />
                        : <Content />
                }
            </React.StrictMode>
        </div>
    );
};


export default App;
