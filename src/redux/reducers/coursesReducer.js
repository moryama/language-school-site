import courseService from '../../services/courseService';


const initialState = [];

const coursesReducer = (state = initialState, action) => {
    switch (action.type) {
    case 'INIT_COURSES':
        return action.payload;
    default:
        return state;
    }
};

// Action creator
const coursesFetched = (courses) => {
    return {
        type: 'INIT_COURSES',
        payload: courses
    };
};

// Thunk function
export const initializeCourses = () => {
    return async (dispatch) => {
        const courses = await courseService.getAll();
        dispatch(coursesFetched(courses));
    };
};


export default coursesReducer;
