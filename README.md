# Website for a language school

An example of a website to promote language courses and events.

With this project I'm experimenting development going **UI first**. 📱 👈 👧

I am also trying **Semantic UI** as an alternative to **Bootstrap**.

## Tools <a name="tools"></a>

- [React 17.0.1](https://reactjs.org/)

- [Redux 4.0.5](https://redux.js.org/) + [react-redux](https://react-redux.js.org/)

- [Create React App](https://github.com/facebook/create-react-app)

- [Semantic UI React](https://react.semantic-ui.com/)

- [react-router-dom](https://www.npmjs.com/package/react-router-dom)


## TODOs <a name="todos"></a>

Check out the [issue tracker](https://gitlab.com/moryama/language-school-site/-/issues).

## Usage <a name="usage"></a>

```
$ git clone https://gitlab.com/moryama/language-school-site.git
```
```
$ npm install
```
```
$ npm run server  // run json server
```
In another terminal and run
```
$ npm start
```